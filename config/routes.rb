Rails.application.routes.draw do

  get 'token/show'

  root :to => 'home#index'

  get 'home/index' => 'home#index'

  get 'home/settings' => 'home#settings'

  get 'home/generate_report' => 'home#generate_report'
  post 'home/generate_report' => 'home#generate_report'

  get 'home/record_settings' => 'home#record_settings'
  post 'home/record_settings' => 'home#record_settings'

  get 'token/secret_code' => 'token#secret_code'

  get 'home/list_products' => 'home#list_products'

  get 'home/list_orders' => 'home#list_orders'

  mount ShopifyApp::Engine, at: '/'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
