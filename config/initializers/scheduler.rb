require 'rufus-scheduler'
require 'date'

s = Rufus::Scheduler.singleton

todays_date = Date.today.strftime("%Y-%m-%d")

s.every '1m' do
  shop_id_array = []
  reports_to_be_shot_today = ReportSetting.where(:execution_date => todays_date)
  if reports_to_be_shot_today.present?
    reports_to_be_shot_today.each do |setting|
      shop_id_array.push(setting.shop_id)

      current_time = Time.now.utc.strftime("%R").to_i
      if setting.frequency == "Daily"
        setting.update_attributes(:execution_date => Time.now.utc.tomorrow.strftime("%d-%m-%Y"))
      elsif setting.frequency == "Weekly"
        setting.update_attributes(:execution_date => Date.today.next_week(day = :monday).strftime("%d-%m-%Y"))
      elsif setting.frequency == "Monthly"
        setting.update_attributes(:execution_date => Date.today.next_month.beginning_of_month.next_week.strftime("%d-%m-%Y"))
      end
    end
    shops = shop_id_array.join("+")
    system("rake generate_report:inventory_reorder["+shops+"]")
  end
end
