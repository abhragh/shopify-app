class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.string :product_name
      t.integer :units_in_hand
      t.integer :units_sold_in_90_days
      t.decimal :estimated_days_till_sold_out

      t.timestamps
    end
  end
end
