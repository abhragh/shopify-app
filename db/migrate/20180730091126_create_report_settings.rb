class CreateReportSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :report_settings do |t|
      t.string :frequency
      t.text :recipients
      t.integer :reorder_days

      t.timestamps
    end
  end
end
