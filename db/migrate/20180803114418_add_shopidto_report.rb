class AddShopidtoReport < ActiveRecord::Migration[5.1]
  def change
    add_column :reports, :shop_id, :integer
  end
end
