class AddExecutionDatetoReprtSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :report_settings, :execution_date, :date
  end
end
