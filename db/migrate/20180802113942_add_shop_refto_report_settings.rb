class AddShopReftoReportSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :report_settings, :shop_id, :integer
  end
end
