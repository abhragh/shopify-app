require 'shopify_api'
require 'net/http'
require 'uri'
require 'cgi'

namespace :generate_report do

  desc "Fetching all prodcucts and orders data from last 90 days and generating Inventory Reorder report"
  task :inventory_reorder, [:shop_ids] => :environment do |task, args|

    shop_id_string = args[:shop_ids]
    shop_id_array = shop_id_string.split("+")
    shop_id_array.each do |shop_id|
      shopify_domain = Shop.find(shop_id.to_i).shopify_domain
      create_report(shopify_domain)
    end
  end

   private
   def parse_time (time)
     if time.nil?
       return Time.now
     end
     if time.is_a? Time
       return time
     end
     if time.is_a? String
       return Time.parse(time)
     end
   end

   def create_report(shopify_domain)

     Report.delete_all   #flushing out all existing data from reports table

     shop = Shop.find_by(shopify_domain: shopify_domain)
     session = ShopifyAPI::Session.new(shop.shopify_domain, shop.shopify_token)
     ShopifyAPI::Base.activate_session(session)

     products = ShopifyAPI::Product.find(:all, params: { limit: 10 })
     products.each do |product|
       report = Report.new
       report.product_name = product.title


       in_hand_quantity = 0
       variants = product.variants
       variants.each do |variant|
         in_hand_quantity = in_hand_quantity + variant.inventory_quantity.to_i
       end

       report.units_in_hand = in_hand_quantity

       orders = ShopifyAPI::Order.find(:all, params: { limit: 10 })
       sold_quantity = 0
       orders.each do |order|
         if order.fulfillment_status == nil
           order.line_items.each do |line_item|
             if line_item.product_id == product.id
               sold_quantity = sold_quantity + line_item.quantity
             end
           end
         end
       end

       report.units_sold_in_90_days = sold_quantity

       if sold_quantity == 0
         estimated_days_till_sold_out = 0
       else
         selling_frequency = (sold_quantity/90).round(2)
         if selling_frequency < 1
           estimated_days_till_sold_out = 0
         else
           estimated_days_till_sold_out = (in_hand_quantity/selling_frequency).round(2)
         end
       end


       report.estimated_days_till_sold_out = 0
       report.shop_id = Shop.find_by_shopify_domain(shopify_domain).id

       report.save
     end
     ReportMailer.send_report.deliver
   end
end
