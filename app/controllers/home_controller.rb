class HomeController < ShopifyApp::AuthenticatedController
  def index
    @products = ShopifyAPI::Product.find(:all, params: { limit: 10 })
    @webhooks = ShopifyAPI::Webhook.find(:all)
  end

  def settings
    @current_shop = ShopifyAPI::Shop.current
    @current_shop_domain = @current_shop.domain
  end

  def record_settings
    report_frequency = params[:frequency]
    reorder_days = params[:reorder_days]
    email_field_data = params[:email]
    current_shop_domain = params[:current_shop_domain]

    if Shop.find_by_shopify_domain(current_shop_domain).present?
      shop_id = Shop.find_by_shopify_domain(current_shop_domain).id
      if ReportSetting.find_by_shop_id(shop_id.to_i).present?  # Existing ReportSetting needs to be updated
        existing_report_settings = ReportSetting.find_by_shop_id(shop_id.to_i)
        calculated_execution_time = calculate_execution_time(report_frequency)
        existing_report_settings.update_attributes(:frequency => report_frequency, :recipients => email_field_data, :reorder_days => reorder_days, :execution_date => calculated_execution_time)
        existing_report_settings.save
      else  # New ReportSetting needs to be inserted
        report_settings = ReportSetting.new
        report_settings.frequency = report_frequency
        report_settings.recipients = email_field_data
        report_settings.reorder_days = reorder_days
        report_settings.shop_id = shop_id
        report_settings.execution_date = calculate_execution_time(report_frequency)
        report_settings.save
      end

      redirect_to home_settings_path(:sucess => "true"), notice: "Settings were updated successfully!" and return

    else
      redirect_to home_settings_path(:sucess => "false"), notice: "Settings cannot be updated! No shop found." and return
    end
  end


  def list_products
    @products = ShopifyAPI::Product.find(:all, params: { limit: 10 })
    render json: @products.first
  end

  def list_orders
    @orders = ShopifyAPI::Order.find(:all, params: { limit: 10 })
    render json: @orders
  end

  def calculate_execution_time(report_frequency)
    current_time = Time.now.utc.strftime("%R").to_i
    if report_frequency == "Daily"
        if current_time < 8    # Report will be shot today itself
          execution_time = Time.now.utc.strftime("%d-%m-%Y")
        else
          execution_time = Time.now.utc.tomorrow.strftime("%d-%m-%Y")
        end
    elsif report_frequency == "Weekly"
      execution_time = Date.today.next_week(day = :monday).strftime("%d-%m-%Y")
    elsif report_frequency == "Monthly"
      execution_time = Date.today.next_month.beginning_of_month.next_week.strftime("%d-%m-%Y")
    end
    return execution_time
  end

end
