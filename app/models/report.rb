class Report < ApplicationRecord
  def self.generate_csv(options = {})
    CSV.generate(options) do |csv|
      column_names = ["product_name","units_in_hand","units_sold_in_90_days","estimated_days_till_sold_out"]
      csv << column_names
      all.each do |report|
        csv << report.attributes.values_at("product_name","units_in_hand","units_sold_in_90_days","estimated_days_till_sold_out")
      end
    end
  end
end
