class Shop < ActiveRecord::Base
  include ShopifyApp::SessionStorage

  has_one :report_setting
end
