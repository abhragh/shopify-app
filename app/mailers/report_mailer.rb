class ReportMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.report_mailer.send_report.subject
  #
  def send_report
    @csv = Report.generate_csv
    attachments['report.csv'] = {mime_type: 'text/csv', content: @csv}
    mail(to: "abhradip.brainium@gmail.com", subject: 'Inventory Reorder Report', body: 'Inventory Reorder report has been generated. PFA')
    Report.delete_all
  end
end
