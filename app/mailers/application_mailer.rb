class ApplicationMailer < ActionMailer::Base
  default from: 'ghosh.abhradip.inventory@gmail.com'
  layout 'mailer'
end
